<?php

namespace Gary\ApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use Gary\BackendBundle\Entity\User;
use Gary\BackendBundle\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{

    /**
     * @Route("/register")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function RegisterAction(Request $request)
    {
        $msgs = [];
        $token = $request->request->get('token', '');
        $provider = $request->request->get('provider', 'facebook');
        $email = $this->get("service.user")->getEmail($token, $provider);
        if ( ! is_array($email)) {
            $user = $this->getDoctrine()->getRepository('GaryBackendBundle:User')->findOneBy(['email' => $email]);
            if ($user == null) {
                $user = $this->get("service.user")->createUser($token, $provider);
                if (is_array($user)) {
                    $msgs = array_merge($msgs, $user);
                }
            }
            if (empty($msgs)) {
                return new JsonResponse(['user' => $user, 'apikey' => $user->getApiKey()]);
            }
        } else {
            $msgs = array_merge($msgs, $email);
        }
        return new JsonResponse(['msgs' => $msgs], 406);
    }

    /**
     * @Route("/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="api_get_user")
     * @Method("GET")
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserAction($id, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if ($id) {
            $user = $em->getRepository("GaryBackendBundle:User")->findOneById($id);
        } else {
            $user = $em->getRepository("GaryBackendBundle:User")->findOneByUsername($this->get('security.context')->getToken()->getUser()->getUsername());
        }

        /** @var User $user */
        if ($user) {
            return new JsonResponse(array(
                'user' => $user
            ));
        } else {
            return new JsonResponse(array(
                'msgs' => array('User not found')
            ), 403);
        }
    }

    /**
     * @Route("/{id}/{action}", requirements={"action" = "follow|unfollow", "id" = "\d+"}, name="api_user_foll")
     * @Method("POST")
     * @param $id
     * @param $action
     * @param Request $request
     * @return JsonResponse
     */
    public function followUserAction($id, $action, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $loggedUser = $em->getRepository("GaryBackendBundle:User")->findOneByUsername($this->get('security.context')->getToken()->getUser()->getUsername());
        $user = $em->getRepository("GaryBackendBundle:User")->findOneById($id);

        /** @var User $user */
        /** @var User $loggedUser */
        if ($user && $loggedUser) {
            if ($action == 'follow') {
                if ( ! in_array($user, $loggedUser->getFollowing()->toArray())) {
                    $loggedUser->addFollowing($user);
                }
            } else {
                $loggedUser->removeFollowing($user);
            }
            $em->persist($loggedUser);
            $em->flush();

            return new JsonResponse(array(
                'result' => true
            ));
        } else {
            return new JsonResponse(array(
                'msgs' => array('User not found')
            ), 403);
        }
    }

    /**
     * @Route("/{id}/video", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="api_user_video")
     * @Method("GET")
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function userVideoAction($id, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if ($id) {
            $user = $em->getRepository("GaryBackendBundle:User")->findOneById($id);
        } else {
            $user = $em->getRepository("GaryBackendBundle:User")->findOneByUsername($this->get('security.context')->getToken()->getUser()->getUsername());
        }

        /** @var User $user */
        if ($user) {
            return new JsonResponse(array(
                'videos' => $user->getVideos()->toArray()
            ));
        } else {
            return new JsonResponse(array(
                'msgs' => array('User not found')
            ), 403);
        }
    }

    /**
     * @Route("/{id}/favorites", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="api_user_video_fevorite")
     * @Method("GET")
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function userFavotiteVideoAction($id, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if ($id) {
            $user = $em->getRepository("GaryBackendBundle:User")->findOneById($id);
        } else {
            $user = $this->getUser();
        }

        /** @var User $user */
        if ($user) {
            $videos = [];
            foreach ($user->getFavorite()->toArray() as $video) {
                /** @var Video $video */
                $video->setIsRated(1);
                $videos[] = $video;
            }
            return new JsonResponse(array(
                'videos' => $videos
            ));
        } else {
            return new JsonResponse(array(
                'msgs' => array('User not found')
            ), 403);
        }
    }

    /**
     * @Route("/{id}/{follow}", requirements={"follow" = "following|followers", "id" = "\d+"}, defaults={"id" = 0}, name="api_user_following")
     * @Method("GET")
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function userFollowingAction($id, $follow, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if ($id) {
            $user = $em->getRepository("GaryBackendBundle:User")->findOneById($id);
        } else {
            $user = $em->getRepository("GaryBackendBundle:User")->findOneByUsername($this->get('security.context')->getToken()->getUser()->getUsername());
        }

        /** @var User $user */
        if ($user) {
            $list = array();
            if ($follow == 'following') {
                $source = $user->getFollowing()->toArray();
            } else {
                $source = $user->getFollowers()->toArray();
            }

            return new JsonResponse(array(
                $follow => $source
            ));
        } else {
            return new JsonResponse(array(
                'msgs' => array('User not found')
            ), 403);
        }
    }
}
