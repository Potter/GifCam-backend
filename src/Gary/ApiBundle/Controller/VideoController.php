<?php

namespace Gary\ApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use Gary\BackendBundle\Entity\User;
use Gary\BackendBundle\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("/video")
 */
class VideoController extends Controller
{
    /**
     * @Route("")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("GaryBackendBundle:User")->findOneByUsername($this->get('security.context')->getToken()->getUser()->getUsername());

        $title = $request->request->get('title', null);
        $description = $request->request->get('description', null);
        $image = $request->files->get('image', null);
        $videoFile = $request->files->get('video', null);
        $latitude = $request->request->get('latitude', null);
        $longitude = $request->request->get('longitude', null);
        $shotsCount = $request->request->get('shotsCount', 0);

        if ($title && $description && $image && $videoFile) {
            $video = new Video();
            $video->setTitle($title);
            $video->setDescription($description);
            $video->setUser($user);
            $video->setFileImage($image);
            $video->setFileVideo($videoFile);
            $video->setLatitude($latitude);
            $video->setLongitude($longitude);
            $video->setShotsCount($shotsCount);
            $em->persist($video);
            $em->flush();
            return new JsonResponse(['video' => $video]);
        } else {
            return new JsonResponse(['msgs' => ['Send all fields']], 403);
        }


    }


    /**
     * @Route("/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="api_get_video")
     * @Method("GET")
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function getVideoAction($id, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository("GaryBackendBundle:Video")->findOneById($id);
        /** @var Video $video */
        if ($video) {
            return new JsonResponse(array(
                'video' => $video
            ));
        } else {
            return new JsonResponse(array(
                'msgs' => array('Video not found')
            ), 403);
        }
    }

    /**
     * @Route("/{id}/{action}", requirements={"action" = "fav|unfav", "id" = "\d+"}, defaults={"id" = 0}, name="api_follow_user")
     * @Method("POST")
     * @param $id
     * @param $action
     * @param Request $request
     * @return JsonResponse
     */
    public function favVideoAction($id, $action, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $loggedUser = $em->getRepository("GaryBackendBundle:User")->findOneByUsername($this->get('security.context')->getToken()->getUser()->getUsername());
        $video = $em->getRepository("GaryBackendBundle:Video")->findOneById($id);

        /** @var Video $video */
        /** @var User $loggedUser */
        if ($video && $loggedUser) {
            if ($action == 'fav') {
                if ( ! in_array($video, $loggedUser->getFavorite()->toArray())) {
                    $loggedUser->addFavorite($video);
                }
                $setFavorite = true;
            } else {
                $loggedUser->removeFavorite($video);
            }
            $em->persist($loggedUser);
            $em->flush();

            // ReCalc Video & User rating
            $totalRating = 0;
            $videoAuthor = $video->getUser();
            foreach ($videoAuthor->getVideos()->toArray() as $video) {
                $totalRating += count($video->getFavoriteUser());
            }
            $videoAuthor->setRating($totalRating);
            $em->persist($videoAuthor);
            $em->flush();

            $video = $em->getRepository("GaryBackendBundle:Video")->findOneById($id);
            if (isset($setFavorite)) {
                $video->setIsRated(1);
            }
            return new JsonResponse(array(
                'video' => $video
            ));
        } else {
            return new JsonResponse(array(
                'msgs' => array('Video not found')
            ), 403);
        }
    }

    /**
     * @Route("/feed", name="api_feed_video")
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function feedVideoAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository("GaryBackendBundle:Video")->feed($request->query->get('count', 20), $request->query->get('offset', 0));
        /** @var User $user */
        $user = $this->getUser();
        $favs = [];
        foreach ($user->getFavorite()->toArray() as $favorite) {
            $favs[] = $favorite->getId();
        }
        /** @var Video $video */
        foreach ($result as $k => $video) {
            if (in_array($video->getId(), $favs)) {
                $result[$k]->setIsRated(1);
            }
        }



        return new JsonResponse(array(
            'videos' => $result
        ));
    }



}
