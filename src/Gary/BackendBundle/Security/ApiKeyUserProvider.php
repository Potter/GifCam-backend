<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 1/26/15
 * Time: 10:00 AM
 */

namespace Gary\BackendBundle\Security;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiKeyUserProvider implements UserProviderInterface
{
    /** @var Registry */
    private $doctrine;

    public function getUserForApiKey($apiKey)
    {
        return $this->getDoctrine()->getRepository('GaryBackendBundle:User')->findOneBy(array('apikey' => $apiKey));
    }

    public function loadUserByUsername($username)
    {
        $criteria = array('username' => $username);
        if ($user = $this->getDoctrine()->getRepository('GaryBackendBundle:User')->findOneBy($criteria)) {
            return $user;
        }

        return new User($username, null, array('ROLE_USER'));
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    /**
     * @return Registry
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @param Registry $doctrine
     */
    public function setDoctrine(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}