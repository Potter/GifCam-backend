<?php

namespace Gary\BackendBundle\Entity;

use Doctrine\ORM\Mapping\ManyToMany;
use Gary\BackendBundle\Entity\Video;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gary\BackendBundle\Entity\Repository\UserRepository")
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser implements \JsonSerializable
{
    const SEX_FEMALE = 0;
    const SEX_MALE = 1;
    const UPLOAD_DIR = '/uploads/photos';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=50, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=50, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     * @Assert\Email()
     * */
    protected $email;

    /**
     * @var string
     * @ Assert\Length(min="8", max="16")
     * */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="apikey", type="string", length=32, nullable=true)
     */
    private $apikey;

    /**
     * @var string
     *
     * @ORM\Column(name="google_id", type="string", length=50, nullable=true)
     */
    private $googleId;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", length=50, nullable=true)
     */
    private $facebookId;

    /**
     * @var UploadedFile $file
     * @Assert\File(maxSize="10M", mimeTypes={"image/jpeg", "image/png"}, mimeTypesMessage="Please upload valid image")
     */
    protected $file;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=50, nullable=true)
     */
    private $photo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     * @Assert\Date()
     */
    private $birthday;

    /**
     * @var integer
     *
     * @ORM\Column(name="sex", type="integer", nullable=true)
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=30, nullable=true)
     */
    private $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="facebookAccessToken", type="string", length=150, nullable=true)
     */
    private $facebookAccessToken;

    /**
     * @var $videos
     *
     *  @ORM\OneToMany(targetEntity="Gary\BackendBundle\Entity\Video", mappedBy="user", cascade={"persist", "remove"})
     */
    private $videos;

    /**
     * @var $favorite
     *
     * @ManyToMany(targetEntity="Gary\BackendBundle\Entity\Video", inversedBy="favorite_user")
     */
    private $favorite;



    /**
     * @var $followers
     *
     * @ManyToMany(targetEntity="Gary\BackendBundle\Entity\User", mappedBy="following")
     */
    private $followers;

    /**
     * @var $following
     *
     * @ManyToMany(targetEntity="Gary\BackendBundle\Entity\User", inversedBy="followers")
     */
    private $following;



    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createdAt", type="datetime")
     */
    protected $createdAt;
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    protected $updatedAt;



    private $files = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->collections = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        $this->username = $this->firstName . ' ' . $this->lastName . ' ' . (($this->email) ? ($this->email) : (time()));

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        $this->username = $this->firstName . ' ' . $this->lastName . ' ' . (($this->email) ? ($this->email) : (time()));
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set apikey
     *
     * @param string $apikey
     * @return User
     */
    public function setApikey($apikey)
    {
        $this->apikey = $apikey;

        return $this;
    }

    /**
     * Get apikey
     *
     * @return string
     */
    public function getApikey()
    {
        return $this->apikey;
    }

    /**
     * Set googleId
     *
     * @param string $googleId
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;

        return $this;
    }

    /**
     * Get googleId
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set file
     *
     * @param UploadedFile $file
     * @return User
     */
    public function setFile($file)
    {
        if(!is_null($file)) {
            $this->setFiles('file');
            $this->file = $file;
            $this->setUpdatedAt(new \DateTime());
        }
        return $this;
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     * @return User
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return integer
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return User
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Add video
     *
     * @param Video $video
     * @return User
     */
    public function addVideo(Video $video)
    {
        $this->videos[] = $video;
        return $this;
    }

    /**
     * Remove video
     *
     * @param Video $video
     */
    public function removeVideo(Video $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function upload()
    {
        foreach($this->files as $field=>$file) {
            $fileName = time() . '_' . md5($this->{$field}->getClientOriginalName()) . '.' . $this->{$field}->getClientOriginalExtension();
            $this->{$field}->move(
                $this->getUploadRootDir(),
                $fileName
            );
            @unlink($this->getUploadRootDir() . DIRECTORY_SEPARATOR . $file);
            $this->photo = $fileName;
        }
    }

    /**
     * @ORM\PreRemove
     */
    public function remove()
    {
        $this->setFiles('file');
        foreach($this->files as $file) {
            @unlink($this->getUploadRootDir() . DIRECTORY_SEPARATOR . $file);
        }
    }

    protected function getUploadDir()
    {
        return $this::UPLOAD_DIR;
    }

    protected function getUploadRootDir()
    {
        return getcwd() . $this->getUploadDir();
    }

    public function setFiles($field)
    {
        $this->files[$field] = $this->{$field};
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getWebPath($field)
    {
        if ($this->$field) {
            return $this->getUploadDir().'/'.$this->$field;
        } else {
            return '/avatarDefault.png';
        }
    }






    /**
     * Add followers
     *
     * @param \Gary\BackendBundle\Entity\User $followers
     * @return User
     */
    public function addFollower(\Gary\BackendBundle\Entity\User $followers)
    {
        $this->followers[] = $followers;

        return $this;
    }

    /**
     * Remove followers
     *
     * @param \Gary\BackendBundle\Entity\User $followers
     */
    public function removeFollower(\Gary\BackendBundle\Entity\User $followers)
    {
        $this->followers->removeElement($followers);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add following
     *
     * @param \Gary\BackendBundle\Entity\User $following
     * @return User
     */
    public function addFollowing(\Gary\BackendBundle\Entity\User $following)
    {
        $this->following[] = $following;



        return $this;
    }

    /**
     * Remove following
     *
     * @param \Gary\BackendBundle\Entity\User $following
     */
    public function removeFollowing(\Gary\BackendBundle\Entity\User $following)
    {
        $this->following->removeElement($following);
    }

    /**
     * Get following
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFollowing()
    {
        return $this->following;
    }

    public function __toString() {
        return $this->username;
    }

    public function getGenders()
    {
        return array(
                self::SEX_FEMALE => 'Female',
                self::SEX_MALE => 'Male'
        );
    }

    public function getGender($sex)
    {
        $genders = $this->getGenders();
        return ((isset($genders[$sex])) ? ($genders[$sex]) : (''));
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Add favorite
     *
     * @param \Gary\BackendBundle\Entity\Video $favorite
     * @return User
     */
    public function addFavorite(\Gary\BackendBundle\Entity\Video $favorite)
    {
        $this->favorite[] = $favorite;

        return $this;
    }

    /**
     * Remove favorite
     *
     * @param \Gary\BackendBundle\Entity\Video $favorite
     */
    public function removeFavorite(\Gary\BackendBundle\Entity\Video $favorite)
    {
        $this->favorite->removeElement($favorite);
    }

    /**
     * Get favorite
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFavorite()
    {
        return $this->favorite;
    }

    function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'photo' => $this->getWebPath('photo'),
            'rating' => $this->getRating(),
            'followers_count' => count($this->getFollowers()),
            'following_count' => count($this->getFollowing()),
            'video_count' => count($this->getVideos())
        );
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string 
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }
}
