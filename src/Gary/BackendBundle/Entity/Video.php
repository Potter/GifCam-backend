<?php

namespace Gary\BackendBundle\Entity;

use Gary\BackendBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Video
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gary\BackendBundle\Entity\Repository\VideoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Video implements \JsonSerializable
{
    const UPLOAD_DIR = '/uploads/videos';

    private $files = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createdAt", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    protected $updatedAt;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
 * @var integer
 *
 * @ORM\Column(name="rating", type="integer", nullable=true)
 */
    private $rating;

    /**
     * @var integer
     *
     * @ORM\Column(name="shotsCount", type="integer", nullable=true)
     */
    private $shotsCount;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @var UploadedFile $fileImage
     * @Assert\File(maxSize="10M", mimeTypes={"image/jpeg", "image/png"}, mimeTypesMessage="Please upload valid image")
     */
    protected $fileImage;

    /**
     * @var UploadedFile $fileVideo
     * @Assert\File(maxSize="50M", mimeTypesMessage="Please upload valid image")
     */
    protected $fileVideo;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /** @var User
     * @ORM\ManyToOne(targetEntity="Gary\BackendBundle\Entity\User", inversedBy="videos")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var $favorite_user
     *
     * @ManyToMany(targetEntity="Gary\BackendBundle\Entity\User", mappedBy="favorite")
     */
    private $favorite_user;

    private $is_rated = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Video
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Video
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Video
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return Video
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Video
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString() {
        return $this->title;
    }

    /**
     * Add favorite_user
     *
     * @param \Gary\BackendBundle\Entity\User $favoriteUser
     * @return Video
     */
    public function addFavoriteUser(\Gary\BackendBundle\Entity\User $favoriteUser)
    {
        $this->favorite_user[] = $favoriteUser;

        return $this;
    }

    /**
     * Remove favorite_user
     *
     * @param \Gary\BackendBundle\Entity\User $favoriteUser
     */
    public function removeFavoriteUser(\Gary\BackendBundle\Entity\User $favoriteUser)
    {
        $this->favorite_user->removeElement($favoriteUser);
    }

    /**
     * Get favorite_user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFavoriteUser()
    {
        return $this->favorite_user;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Video
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return Video
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Video
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Video
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function upload()
    {
        foreach($this->files as $field=>$file) {
            $fileName = time() . '_' . md5($this->{$field}->getClientOriginalName()) . '.' . $this->{$field}->getClientOriginalExtension();
            $this->{$field}->move(
                $this->getUploadRootDir(),
                $fileName
            );



            if (strpos(strtolower($field), 'image')) {
                $this->image = $fileName;
            }
            if (strpos(strtolower($field), 'video')) {
                $this->video = $fileName;
            }

            @unlink($this->getUploadRootDir() . DIRECTORY_SEPARATOR . $file);
        }
    }

    /**
     * @ORM\PreRemove
     */
    public function remove()
    {
//        $this->setFiles('file');
//        foreach($this->files as $file) {
//            @unlink($this->getUploadRootDir() . DIRECTORY_SEPARATOR . $file);
//        }
    }

    protected function getUploadDir()
    {
        return $this::UPLOAD_DIR;
    }

    protected function getUploadRootDir()
    {
        return getcwd() . $this->getUploadDir();
    }

    public function setFiles($field)
    {
        $this->files[$field] = $this->{$field};
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getWebPath($field)
    {
        if ($this->$field) {
            return $this->getUploadDir().'/'.$this->$field;
        } else {
            return '/avatarDefault.png';
        }
    }



    /**
     * Set file
     *
     * @param UploadedFile $file
     * @return Video
     */
    public function setFileImage($file)
    {
        if(!is_null($file)) {
            $this->setFiles('fileImage');
            $this->fileImage = $file;
        }
        return $this;
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getFileImage()
    {
        return $this->fileImage;
    }

    /**
     * Set file
     *
     * @param UploadedFile $file
     * @return Video
     */
    public function setFileVideo($file)
    {
        if(!is_null($file)) {
            $this->setFiles('fileVideo');
            $this->fileVideo = $file;
        }
        return $this;
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getFileVideo()
    {
        return $this->fileVideo;
    }

    public function setIsRated($rated)
    {
        $this->is_rated = $rated;
    }

    public function getIsRated()
    {
        return $this->is_rated;
    }


    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'created' => $this->getCreatedAt()->getTimestamp(),
            'url' => $this->getWebPath('video'),
            'thumb' => $this->getWebPath('image'),
            'rating' => count($this->getFavoriteUser()),
            'latitude' => $this->getLatitude(),
            'longitude' => $this->getLongitude(),
            'user' => $this->getUser(),
            'shotsCount' => $this->getShotsCount(),
            'israted' => $this->is_rated
        );
    }

    /**
     * Set shotsCount
     *
     * @param integer $shotsCount
     * @return Video
     */
    public function setShotsCount($shotsCount)
    {
        $this->shotsCount = $shotsCount;

        return $this;
    }

    /**
     * Get shotsCount
     *
     * @return integer 
     */
    public function getShotsCount()
    {
        return $this->shotsCount;
    }
}
