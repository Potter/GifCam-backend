$(document).ready(function(){
    $('#dashform .dashchanger').change(function(){
        $('#dashform').submit();
    });
    $('#wordsForm .dashchanger').change(function(){
        $('#wordsForm').submit();
    });
});

function updatePhoto(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.user_profile.edit .photo img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }

}