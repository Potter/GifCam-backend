<?php

namespace Gary\BackendBundle\Service;

use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use FOS\UserBundle\Doctrine\UserManager;
use Gary\BackendBundle\Controller\Util;
use Gary\BackendBundle\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Validator;
use Doctrine\Bundle\DoctrineBundle\Registry;


/**
 * Description of FacebookService
 *
 * @author igor
 */
class FacebookService
{
    protected $credentials;

    public function __construct($credentials, $userManager, $securityContext, Registry $doctrine)
    {
        $this->credentials = $credentials;
        $this->securityContext = $securityContext;
        $this->userManager = $userManager;
        $this->em = $doctrine->getManager();
    }

    public function getEmail($token)
    {
        $msgs = [];

        FacebookSession::setDefaultApplication($this->credentials['app_id'], $this->credentials['app_secret']);
        $session = new FacebookSession($token);
        try {
            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
        } catch (\Exception $e) {
            $msgs[] = $e->getMessage();
        }
        if (empty($msgs)) {
            $graphObject = $response->getGraphObject();
            $responseArray = $graphObject->asArray();
            return $responseArray['email'];
        }
        return $msgs;
    }

    public function createUser($token)
    {
        $msgs = [];
        $user = new User();
        FacebookSession::setDefaultApplication($this->credentials['app_id'], $this->credentials['app_secret']);
        $session = new FacebookSession($token);
        try {
            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            $graphObject = $response->getGraphObject();
            $responseArray = $graphObject->asArray();
            $user->setFirstName($responseArray['first_name']);
            $user->setLastName($responseArray['last_name']);
            $user->setEmail($responseArray['email']);
            $user->setSex($responseArray['gender']);
            $user->setLocation($responseArray['locale']);
            $user->setBirthday(new \DateTime($responseArray['birthday']));
            $user->setPlainPassword(md5($responseArray['email'] . rand(1, 999999) . $responseArray['first_name'] . rand(1, 999999) . time() . rand(1, 999999)));
            $user->setApiKey(md5($user->getPlainPassword()));
            $user->setFacebookId($responseArray['id']);
            $user->setFacebookAccessToken($token);


            if ($responseArray['id']) {
                $imageUrl = 'http://graph.facebook.com/' . $responseArray['id'] . '/picture?type=large&width=720&height=720';
                $filename = getcwd() . Util::TMP_UPLOAD_DIR . '/' . $responseArray['id'] . '.jpg';
                file_put_contents($filename, file_get_contents($imageUrl));
                $file = new UploadedFile($filename, 'avatar.jpg', 'image/jpeg', filesize($filename), null, true);
                $user->setFile($file);
            }

            $user->setEnabled(true);

            $this->em->persist($user);
            $this->em->flush();
            if (empty($msgs)) {
                return $user;
            }
        } catch (\Exception $e) {
            $msgs[] = $e->getMessage();
        }
        return $msgs;
    }
}