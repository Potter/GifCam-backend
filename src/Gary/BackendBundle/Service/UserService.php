<?php

namespace Gary\BackendBundle\Service;

use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use FOS\UserBundle\Doctrine\UserManager;
use Gary\BackendBundle\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Validator;
use Doctrine\Bundle\DoctrineBundle\Registry;


/**
 * Description of FacebookService
 *
 * @author igor
 */
class UserService
{
    protected $credentials;

    public function __construct($userManager, $securityContext, Registry $doctrine, FacebookService $facebook)
    {
        $this->securityContext = $securityContext;
        $this->userManager = $userManager;
        $this->em = $doctrine->getManager();
        $this->facebook = $facebook;
//        $this->google = $google;
//        $this->mcfedr_aws_push_devices = $mcfedr_aws_push_devices;
    }

    public function createUser($token, $provider)
    {
        $user = null;
        switch ($provider) {
            case 'facebook': {
                $user = $this->facebook->createUser($token);
            } break;
            case 'google': {
                return ['not implemented yet'];
//                $user = $this->google->createUser($token);
            } break;
        }
        if (! $user) {
            return ['Something went wrong'];
        }
        return $user;
    }



    public function getEmail($token, $provider)
    {

        $email = '';
        switch ($provider) {
            case 'facebook': {
                $email = $this->facebook->getEmail($token);
            } break;
            case 'google': {
                return ['not implemented yet'];
//                $email = $this->google->getEmail($token);
            } break;
        }
        return $email;
    }
}